Installation
============

Firstly you will need to have Python 3 installed. You can do this by using `Anaconda <https://www.continuum.io/downloads>`_ for a complete setup or the default implementation of `Python <https://www.python.org/downloads/>`_.

The mango source can be downloaded here: `Mango <https://gitlab.com/mangocode/Mango>`_

Quickstart
----------

::

  git clone https://gitlab.com/mangocode/Mango.git@master

and install the resulting folder with pip::

  pip install -U -e [mango folder]


Test
----

This should show you the internal help once installed

::

 mango -h

The test suite can also be run on Linux with pytest

::

  pytest [mango folder]/mango/tests


Dependencies
------------

Minimal requirements:

* Python 3.6+
* `NumPy <https://www.numpy.org/>`_
* `SciPy <https://www.scipy.org/>`_
* `Matplotlib <https://matplotlib.org>`_

The above requirements are installed automatically when using pip.

Recommended Modules:

* `pyFFTW <https://github.com/pyFFTW/pyFFTW>`_
* `Pytables <http://www.pytables.org/usersguide/installation.html>`_

`pyFFTW <https://github.com/pyFFTW/pyFFTW>`_ will speedup the fourier transform with a direct wrapper around the C `FFTW library <http://www.fftw.org>`_.
`Pytables <http://www.pytables.org/usersguide/installation.html>`_ provides an interface for hdf5 files. To use this you will also need to install the `hdf5 library <https://www.hdfgroup.org/HDF5/>`_. This module allows for compressed and much more portable files.

::

 pip install tables pyfftw

Optional Modules:

* `mpi4py <https://mpi4py.readthedocs.io>`_ for massively parallel simulation
* `Inquirer <https://github.com/magmax/python-inquirer>`_ for more interactive postprocessing
* `Vispy <https://vispy.org>`_  and `imageio <https://imageio.github.io>`_ are needed to use the builtin particle visualiser, imageio-ffmpeg is needed for video production.

::

 pip install mpi4py vispy imageio imageio-ffmpeg

Testing Modules:


* `pytest <https://docs.pytest.org/en/latest/>`_ to be able to run the test suite along with `wurlitzer <https://github.com/minrk/wurlitzer>`_.

::

  pip install pytest wurlitzer


It should be noted many of the tests in the test suite may fail on MacOS.
